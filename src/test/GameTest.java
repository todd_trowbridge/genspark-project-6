import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    Game game;

    @Test
    void gameStory() {
        game = new Game();
        assertEquals("You've entered a land of wealth and danger.\n"
                + "You may explore the land and discover it's treasures...\n"
                + "But be careful, you may encounter deadly Goblins.\n", game.gameStory());
    }
}