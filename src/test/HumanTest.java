import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

class HumanTest {

    Human human;

    @BeforeEach
    void setUp() {
        human = new Human();
    }

    @Test
    void setHealth() {
        assertEquals(100, human.getHealth());
    }

    @Test
    void getHealth() {
        human.setHealth(10);
        assertEquals(10,human.getHealth());
    }

    @Test
    void getName() {
        human.setName("Steve");
        assertEquals("Steve", human.getName());
    }

    @Test
    void setName() {
        human.setName("test name");
        assertEquals("test name", human.getName());
    }

    @Test
    void testToString() {
        human.setHealth(10);
        assertEquals("Player Health: 10", human.toString());
    }

    @Test
    void greetPlayer() {
        human.setName("test name");
        assertEquals("Good luck test name!", human.greetPlayer());
    }
}