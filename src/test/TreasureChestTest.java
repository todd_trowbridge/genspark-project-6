import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TreasureChestTest {

    TreasureChest treasureChest;

    @Test
    void getHealth() {
        treasureChest = new TreasureChest();
        treasureChest.setHealth(9);
        assertEquals(9, treasureChest.getHealth());
    }

    @Test
    void generateRandomHealth() {
        treasureChest = new TreasureChest();
        if (treasureChest.getHealth() >= 0 && treasureChest.getHealth() <= 10){
            assertTrue(true);
        } else {
            fail();
        }
    }
}