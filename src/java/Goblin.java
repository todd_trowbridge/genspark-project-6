import java.util.Random;

public class Goblin {
    // health
    private int health = 20;
    // name
    private String name;
    // damage dealt
    private int damage;

    // getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    // methods

    // generate random damage value
    public void generateRandomDamage(){
        double damage = Math.random()*10;
        setDamage((int)damage);
    }

    // deal damage
    public void dealDamage(Human human){
        // create random object
        Random willDealDamage = new Random();
        // generate random boolean
        boolean randomBoolean = willDealDamage.nextBoolean();
        if (randomBoolean){
            // generate damage
            generateRandomDamage();
            // get human health
            int humanHealth = human.getHealth();
            // subtract goblin damage from human health
            human.setHealth(humanHealth - getDamage());
            System.out.printf("You've been hurt, they did %s damage.", getDamage());
        } else {
            System.out.println("You blocked the attack!");
        }
    }

    // generate goblin name

    // overrides
    public String toString(){
        return String.format("Goblin Health: %s", this.health);
    }

}
