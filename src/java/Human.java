import java.util.Random;

public class Human {
    // health
    private int health = 100;
    // name
    private String name;
    // damage
    private int damage;

    // getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    // methods

    // generate random damage value
    public void generateRandomDamage(){
        double damage = Math.random()*10;
        setDamage((int)damage);
    }

    // deal damage
    public void dealDamage(Goblin goblin){
        // create random object
        Random willDealDamage = new Random();
        // generate random boolean
        boolean randomBoolean = willDealDamage.nextBoolean();
        if (randomBoolean){
            // generate damage
            generateRandomDamage();
            // get human health
            int goblinHealth = goblin.getHealth();
            // subtract goblin damage from human health
            goblin.setHealth(goblinHealth - getDamage());
        } else {
            System.out.println("They blocked the attack!");
        }
    }

    // greet player by name
    public String greetPlayer(){
        String output = "Good luck " + this.name + "!";
        System.out.println(output);
        return output;
    }
        // deal damage (possibly in interface?)


    // overrides
    public String toString(){
        return String.format("Player Health: %s", this.health);
    }

    // constructor

}
