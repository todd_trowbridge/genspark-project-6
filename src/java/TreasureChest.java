public class TreasureChest {
    private int health = 0;

    public TreasureChest() {
        generateRandomHealth();
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void generateRandomHealth(){
        double health = Math.random()*10;
        this.health = (int)health;
    }
}
