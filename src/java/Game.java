import java.util.Scanner;

public class Game {
    Human player = new Human();
    Map map = new Map();
    // start the game
    public void start(){
        // print backstory
        gameStory();
        // ask for name
        askForName();
        // begin game loop
        gameLoop();
    }

    // describe the game and output to console
    public String gameStory(){
        String output = "You've entered a land of wealth and danger.\n"
                + "You may explore the land and discover it's treasures...\n"
                + "But be careful, you may encounter deadly Goblins.\n";
        System.out.println(output);
        return output;
    }

    // ask user for name
    public void askForName(){
        // create scanner
        Scanner getInput = new Scanner(System.in);
        // create question
        String question = "What is your name?";
        // ask question
        System.out.println(question);
        // get name
        String input = getInput.nextLine();
        // set name
        player.setName(input);
        // greet the player by name
        player.greetPlayer();
    }

    // generate map

    // begin game loop
    public void gameLoop(){
        map.generateGrid();
        map.printMapContents();
        System.out.println("loop finished");
    }
        // tell player where they are

        // ask if player wants to move north/s/e/w

        // check if goblin is in area and initiate combat

        // check if player is alive

        // if player is alive restart loop
}
