import java.util.Random;

public class GridSquare {
    Goblin goblin;
    TreasureChest treasureChest;
    // create random object
    Random random = new Random();
    // generate random booleans
    boolean randomBooleanGoblin = random.nextBoolean();
    boolean randomBooleanTreasure = random.nextBoolean();

    // generate grid-square contents
    public void generateGridSquare(){

        if (randomBooleanGoblin){
            goblin = new Goblin();
        }

        if(randomBooleanTreasure){
            treasureChest = new TreasureChest();
        }
    }
    public String toString(){
        return String.format("GridSquare contains - Goblin: %s - Treasure: %s", randomBooleanGoblin, randomBooleanTreasure);
    }
}
