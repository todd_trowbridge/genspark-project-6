import java.util.ArrayList;

public class Map {
    // player location
    private int playerLocation = 4;
    // map
    private ArrayList<GridSquare> map = new ArrayList<GridSquare>();
    // grid size
    int XY = 3;

    // generate grid
    public void generateGrid(){
        for (int i = XY * XY; i > 0; i--){
            // add grid-square
            GridSquare gridSquare = new GridSquare();
            // generate gridSquare
            gridSquare.generateGridSquare();
            // add gridSquare
            map.add(gridSquare);
        }
    }
    
    public void printMapContents(){
        map.forEach((gridSquare -> System.out.println(gridSquare.toString())));
    }

    public ArrayList<GridSquare> getMap() {
        return map;
    }

    public void setMap(ArrayList<GridSquare> map) {
        this.map = map;
    }

    public int getPlayerLocation() {
        return playerLocation;
    }

    public void setPlayerLocation(int playerLocation) {
        this.playerLocation = playerLocation;
    }

    // move method
        // check which moves are possible
        // ask user if they want to

}
